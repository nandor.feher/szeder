const axios = require('axios');

exports.getWeather = async (req, res, next) => {
  const lat = +req.query.lat;
  const lon = +req.query.lon;
  
  if(lat && lon) {
    axios.get('https://api.openweathermap.org/data/2.5/onecall?lat=' + lat + '&lon=' + lon + '&exclude=hourly,minutely&appid=6b7db5feef6a3d47cc8e602ca7432185').then(resp => {

      // Get desires weather data from response.
      fetchedWeather = {
        temperature: resp.data.current.temp,
        condition: resp.data.current.weather[0].main,
        humidity: resp.data.current.humidity,
        wind: resp.data.current.wind_speed,
        uvi: resp.data.current.uvi,
        visibility: resp.data.current.visibility,
        clouds: resp.data.current.clouds,
        timezone: resp.data.timezone
      };

      // Make empty array.
      forecast = [];

      // Fill array with data from the response.
      resp.data.daily.forEach(item => {
        forecast.push({
          time: item.dt,
          temperature: item.temp.day,
          condition: item.weather[0].main
        });
      });

      // We actually don't need the first one in this example.
      forecast.shift();

      res.status(200).json({
        weather: fetchedWeather,
        forecast: forecast
      });
    });
  } else res.status(401).json({
    message: "Weather fetch failed"
  });
};