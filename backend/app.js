const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const cors = require('cors');

// Defining routes.
const userRoutes = require("./routes/user");
const cityRoutes = require('./routes/city');
const weatherRoutes = require('./routes/weather');
const favoritesRoutes = require('./routes/favorites');
const settingsRoutes = require('./routes/settings');

const app = express();

// Connect to Db.
mongoose
  .connect("mongodb+srv://dbAdmin:dbAdmin@cluster0.dhggh.mongodb.net/Cluster0?retryWrites=true&w=majority", { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true,})
  .then(() => {
    console.log("Connected to database!");
  })
  .catch(() => {
    console.log("Connection failed!");
  });

// Used for request parsing.
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
// CORS settings
app.use(cors({origin:true,credentials: true}));

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PATCH, PUT, DELETE, OPTIONS"
  );
  next();
});

// Using routes.
app.use("/api/user", userRoutes);
app.use("/api/city", cityRoutes);
app.use("/api/weather", weatherRoutes);
app.use("/api/favorites", favoritesRoutes);
app.use("/api/settings", settingsRoutes);

module.exports = app;
