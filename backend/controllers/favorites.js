const axios = require('axios');
const User = require("../models/user");
const City = require("../models/city");
const jwt = require("jsonwebtoken");

exports.addFavorite = async (req, res, next) => {
  try {

    // Getting the user from the token
    const token = req.headers.authorization.split(" ")[1];
    const decodedToken = jwt.verify(token, 'b96b901c731d3fc66adadbac51a97674');
    const fetchedUser = await User.findOne({ _id: decodedToken.userId });
  
    if (!fetchedUser) {
      return res.status(401).json({
        message: "No matching user"
      });
    }

    // Constructing the favorite
    const favorite = {
      name: req.body.name,
      lat: +req.body.latitude,
      lon: +req.body.longitude
    };

    // If array already contains item (technically not possible without 'hacky' requests)
    if (fetchedUser.favorites.some(item => item.name === favorite.name)) {
      return res.status(418).json({
        message: "Cannot add item twice"
      });
    }

    // Adding the item and saving the Schema
    fetchedUser.favorites.push(favorite);
    fetchedUser.save();

    // Response
    return res.status(201).json({
      message: "Favorite added"
    });

  } catch (error) {
    res.status(500).json({
      message: "Creating a favorite failed!"
    });
  }
};

exports.getFavorites = async (req, res, next) => {

  try {
    // Getting the user from the token
    const token = req.headers.authorization.split(" ")[1];
    const decodedToken = jwt.verify(token, 'b96b901c731d3fc66adadbac51a97674');
    const fetchedUser = await User.findOne({ _id: decodedToken.userId });
  
    if (!fetchedUser) {
      return res.status(401).json({
        message: "No matching user"
      });
    }

    if(fetchedUser.favorites.length === 0) {
      return res.status(404).json({
        message: "No favorites found"
      });
    }

    // Return favorites.
    return res.status(200).json(fetchedUser.favorites);

  } catch (error) {
    res.status(500).json({
      message: "Getting favorites failed!"
    });
  }
};


exports.getFavorite = async (req, res, next) => {

  try {
    // Getting the user from the token
    const token = req.headers.authorization.split(" ")[1];
    const decodedToken = jwt.verify(token, 'b96b901c731d3fc66adadbac51a97674');
    const fetchedUser = await User.findOne({ _id: decodedToken.userId });
  
    if (!fetchedUser) {
      return res.status(401).json({
        check: false
      });
    }

    // get city name from request.
    const cityName = req.params.cityName;

    fetchedUser.favorites.forEach(item => {
      // If city is in favorites.
      if (item.name === cityName) {
        return res.status(200).json({
          check: true
        });
      }
    });

  } catch (error) {
    res.status(500).json({
      check: false
    });
  }
};


exports.deleteFavorite = async (req, res, next) => {
  try {
    // Getting the user from the token
    const token = req.headers.authorization.split(" ")[1];
    const decodedToken = jwt.verify(token, 'b96b901c731d3fc66adadbac51a97674');
    const fetchedUser = await User.findOne({ _id: decodedToken.userId });
  
    if (!fetchedUser) {
      return res.status(401).json({
        message: "No matching user"
      });
    }


    // Delete every favorite (though there is probably a better way to do this)
    if (req.params.cityName === undefined) {
      fetchedUser.favorites = [];
      await fetchedUser.save();
      return res.status(200).json({
        message: "Deleted favorites"
      });
    }

    // Retrieve favorite using the given parameter
    const fetchedFavorite = fetchedUser.favorites.filter(item => item.name === req.params.cityName)[0];
    // Index of fetched item in array
    const index = fetchedUser.favorites.indexOf(fetchedFavorite);

    if (index === -1) {
      // If array doesn't contain item
      return res.status(404).json({
        message: "No item found"
      });
    } else {
      // Delete and save.
      fetchedUser.favorites.splice(index, 1);
      await fetchedUser.save();
      res.status(200).json({
        message: "Deleted"
      });
    }

  } catch (error) {
    res.status(500).json({
      message: "Deleting favorite failed!"
    });
  }
};