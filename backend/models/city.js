const mongoose = require("mongoose");

const citySchema = mongoose.Schema({
  id: String,
  name: String
});

module.exports = mongoose.model("City", citySchema);