import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { City } from 'src/app/models/city.model';
import { CitiesService } from 'src/app/services/cities.service';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainPageComponent implements OnInit, OnDestroy {

  // This is the city that we use for the weather search.
  inputCity = null;

  // Subscriptions
  currentCitySub: Subscription;

  // Observables
  // inputCity$ = Observable<City>;

  constructor(
    private citiesService: CitiesService,
    private cdr: ChangeDetectorRef
    ) { }

  ngOnInit(): void {

    // Subscribe to the current city.
    this.currentCitySub = this.citiesService.currentCity.subscribe((city: City) => {
      this.inputCity = city;
      this.cdr.markForCheck();
    });
  }

  ngOnDestroy(): void {
    this.currentCitySub.unsubscribe();
  }
}
