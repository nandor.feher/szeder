import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Observable, Subscription } from 'rxjs';
import { FavoritesService } from 'src/app/services/favorites.service';
import { SettingsService } from 'src/app/services/settings.service';
import { WeatherService } from 'src/app/services/weather.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SettingsComponent implements OnInit, OnDestroy {

  // Default settings.
  settingUnit = 0;
  settingDarkModeEnabled = false;
  settingZenModeEnabled = false;
  backgroundColor = '';

  // Condition for the loading GIF.
  isLoading = false;

  // Subscriptions
  settingsSub: Subscription;
  darkModeSub: Subscription;

  // Observables
  unit$: Observable<number>;
  darkMode$: Observable<boolean>;
  zenMode$: Observable<boolean>;
  backgroundColor$: Observable<string>;

  constructor(
    private titleService: Title,
    public weatherService: WeatherService,
    private settingsService: SettingsService,
    private favoritesService: FavoritesService,
    private cdr: ChangeDetectorRef
    ) { }

  ngOnInit(): void {
    // Show the loading GIF.
    this.isLoading = true;

    // Set title of page using the built in Title service.
    this.titleService.setTitle('SETTINGS | SZEDER');

    // Request settings from backend.
    this.settingsSub = this.settingsService.getSettings().subscribe(response => {
      this.settingUnit = response.unit;
      this.settingDarkModeEnabled = response.darkMode;
      this.settingZenModeEnabled = response.zenMode;
      // Toggle dark mode on the UI.
      this.settingsService.toggleDarkMode(response.darkMode);

      // Hide the loading GIF.
      this.isLoading = false;

      this.cdr.markForCheck();
    });

    // Setting the observable
    this.backgroundColor$ = this.weatherService.currentBackground;

    // If dark mode state changes, update the UI.
    this.darkModeSub = this.settingsService.darkMode.subscribe(value => {
      this.settingDarkModeEnabled = value;
    });
  }

  toggleUnitSetting(): void {
    // Go from 0 to 1 or vice versa.
    this.settingUnit = Math.abs(this.settingUnit - 1);
    this.settingsService.setSetting('unit', this.settingUnit);
  }

  toggleDarkMode(): void {
    this.settingsService.toggleDarkMode();
    this.settingsService.setSetting('darkMode', this.settingDarkModeEnabled);
  }

  toggleZenMode(): void {
    this.settingZenModeEnabled = !this.settingZenModeEnabled;
    this.settingsService.setSetting('zenMode', this.settingZenModeEnabled);
  }

  deleteFavorites(): void {
    this.favoritesService.deleteFavorites();
  }

  openWeatherMap(): void {
    window.open('https://openweathermap.org/', '_blank');
  }

  ngOnDestroy(): void {
    this.settingsSub.unsubscribe();
    this.darkModeSub.unsubscribe();
  }

}
