const express = require("express");
const router = express.Router();
const FavoritesController = require('../controllers/favorites');
const checkAuth = require("../middleware/check-auth");

router.get("", checkAuth, FavoritesController.getFavorites);
router.post("", checkAuth, FavoritesController.addFavorite);
router.delete("", checkAuth, FavoritesController.deleteFavorite);
router.delete("/:cityName", checkAuth, FavoritesController.deleteFavorite);

module.exports = router;
