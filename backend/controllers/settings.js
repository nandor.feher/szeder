const axios = require('axios');
const User = require("../models/user");
const jwt = require("jsonwebtoken");

exports.getSettings = async (req, res, next) => {
  try {

    // Getting the user from the token
    const token = req.headers.authorization.split(" ")[1];
    const decodedToken = jwt.verify(token, 'b96b901c731d3fc66adadbac51a97674');
    const fetchedUser = await User.findOne({ _id: decodedToken.userId });
  
    if (!fetchedUser) {
      return res.status(401).json({
        message: "No matching user"
      });
    }

    // Constructing the settings
    const settings = fetchedUser.settings;

    // Response
    return res.status(201).json(settings);

  } catch (error) {
    res.status(500).json({
      message: "Getting settings failed!"
    });
  }
};

exports.setSetting = async (req, res, next) => {
  try {

    // Getting the user from the token
    const token = req.headers.authorization.split(" ")[1];
    const decodedToken = jwt.verify(token, 'b96b901c731d3fc66adadbac51a97674');
    const fetchedUser = await User.findOne({ _id: decodedToken.userId });
  
    if (!fetchedUser) {
      return res.status(401).json({
        message: "No matching user"
      });
    }

    // Get setting from request.
    const setting = req.body.setting;

    if (setting === 'unit') {
      fetchedUser.settings.unit = +req.body.value;
    }

    if (setting === 'darkMode') {
      fetchedUser.settings.darkMode = req.body.value;
    }

    if (setting === 'zenMode') {
      fetchedUser.settings.zenMode = req.body.value;
    }

    if (setting === 'deleteFavorites') {
      fetchedUser.favorites = [];
    }

    // Saving the user
    fetchedUser.save();

    // Response
    return res.status(201).json({
      message: "Setting modified"
    });

  } catch (error) {
    res.status(500).json({
      message: "Modifying setting failed!"
    });
  }
};