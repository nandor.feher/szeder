import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { City } from '../models/city.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CitiesService {

  private inputCity = new BehaviorSubject(null);
  currentCity = this.inputCity.asObservable();
  backendUrl = environment.backend_url;

  constructor(
    private http: HttpClient
  ) { }

  // Autocomplete
  getCitiesBySubstring(substring: string): Observable<City> {
    return this.http.get<City>(this.backendUrl + '/city?search=' + substring);
  }

  // Get city from wither name or coords.
  getCity(name: string, lat?: number, lon?: number): Observable<City> {
    if (lat !== undefined && lon !== undefined) {
      return this.http.get<City>(this.backendUrl + '/city?lat=' + lat + '&lon=' + lon);
    } else {
      // returns 500 code
      return this.http.get<City>(this.backendUrl + '/city?search=' + name);
    }
  }

  changeInputCity(city: City): void {
    this.inputCity.next(city);
  }

}
