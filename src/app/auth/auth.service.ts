import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of, Subject } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // Auth status of user.
  private isAuthenticated = false;
  // Stores the auth status of the user.
  private authStatusListener = new Subject<boolean>();
  backendUrl = environment.backend_url;

  constructor(
    private http: HttpClient,
    private router: Router
    ) {}

  // Returns the JWT token from the local storage.
  getToken(): string {
    return localStorage.getItem('token');
  }

  // Returns if the user is authenticated.
  getIsAuth(): boolean {
    return this.isAuthenticated;
  }

  // Returns an Observable that emits the auth status of the user upon subscription.
  getAuthStatusListener(): Observable<boolean> {
    return this.authStatusListener.asObservable();
  }

  // Sends a post request to the '/user/signup' endpoint containing the user's data.
  createUser(email: string, password: string): Observable<number> {
    return this.http.post<number>(this.backendUrl + '/user/signup', { email, password }).pipe(
      catchError(err => {
        this.authStatusListener.next(false);
        return of(err.status);
      })
    );
  }

  // Sends a post request containing the login data.
  login(email: string, password: string): Promise<number> {
    return new Promise((resolve, reject) => {
      this.http.post<{ token: string }>(this.backendUrl + '/user/login', { email, password })
        .subscribe(response => {
          // Extract the token from the response.
          const token = response.token;
          // If token exists.
          if (token) {
            // Save the received token to the local storage.
            localStorage.setItem('token', token);
            // Set auth status to true.
            this.isAuthenticated = true;
            this.authStatusListener.next(true);
            // Go to the weather page.
            this.router.navigate(['/']);
          }
          resolve(200);
        }, error => {
          // Set the auth status to false.
          this.authStatusListener.next(false);
          resolve(error.status);
        }
      );
    });
  }

  logout(): void {
    // Remove the token from the local storage.
    localStorage.removeItem('token');
    // Set auth status to false.
    this.isAuthenticated = false;
    this.authStatusListener.next(false);
    // Go to the weather page.
    this.router.navigate(['/']);
  }
}
