import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignInComponent } from './sign-in/sign-in.component';

const routes: Routes = [
  // Sign in and login are in the same component, on the same page just for simplicity and ease of user navigation.
  { path: 'signin', component: SignInComponent },
];

@NgModule({
    // Since this a lazy loaded component, we have to import it this way.
    imports: [
      RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
  })
  export class AuthRoutingModule {}
