export interface WeatherData {
  weather: {
    temperature: number,
    condition: string,
    humidity: number,
    wind: number,
    uvi: number,
    visibility: number,
    clouds: number,
    timezone: string
  };
  forecast: [{
    time: number,
    temperature: number,
    condition: string
  }];
}
