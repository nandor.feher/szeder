const axios = require('axios');
const { stringify } = require('querystring');

exports.getCity = async (req, res, next) => {

  try {
    const name = req.query.search;
    const lat = +req.query.lat;
    const lon = +req.query.lon;

    var fetchedCity;

    // If coods are given as params.
    if(name !== undefined) {
      const resp = await axios.get('https://api.locationiq.com/v1/autocomplete.php?key=b6ad81335a08bd&q=' + name + '&limit=1&normalizecity=1&tag=place%3Acity&dedupe=1');
      const item = resp.data[0];
      // Set city data using the response.
      fetchedCity = {
        name: item.display_place,
        lat: item.lat,
        lon: item.lon
      };
      // Send back city.
      res.status(200).json(fetchedCity);

    // If city name is in params.
    } else {
      const resp = await axios.get('https://eu1.locationiq.com/v1/reverse.php?key=b6ad81335a08bd&lat=' + lat + '&lon=' + lon + '&format=json');
      const item = resp.data;
      fetchedCity = {
        name: item.address.city,
        lat: item.lat,
        lon: item.lon
      };
      res.status(200).json(fetchedCity);
      
    }
  } catch (error) {
    res.status(500).json({
      message: "Fetching city failed!"
    });
  }
}