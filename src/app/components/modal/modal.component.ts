import { Component, ViewEncapsulation, ElementRef, Input, OnInit, OnDestroy } from '@angular/core';

import { ModalService } from './../../services/modal.service';

@Component({
    selector: 'app-modal-box',
    templateUrl: 'modal.component.html',
    styleUrls: ['modal.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ModalComponent implements OnInit, OnDestroy {
    @Input() id: string;
    private element: any;

    constructor(
      private modalService: ModalService,
      private el: ElementRef
      ) {
      this.element = el.nativeElement;
    }

    ngOnInit(): void {
      // ensure id attribute exists
      if (!this.id) {
        console.error('We found an error.');
        return;
      }

      // move element to bottom of page (just before </body>) so it can be displayed above everything else
      document.body.appendChild(this.element);

      // close modal on background click
      this.element.addEventListener('click', el => {
        if (el.target.className === 'modal-box-wrapper') {
          this.close();
        }
      });

      // add self (this modal instance) to the modal service so it's accessible from controllers
      this.modalService.add(this);
    }

    // Open modal
    open(): void {
        this.element.style.display = 'block';
        document.body.classList.add('modal-box-open');
    }

    // Close modal
    close(): void {
        this.element.style.display = 'none';
        document.body.classList.remove('modal-box-open');
    }

    // remove self from modal service when component is destroyed
    ngOnDestroy(): void {
        this.modalService.remove(this.id);
        this.element.remove();
    }
}