const express = require("express");
const router = express.Router();
const SettingsController = require('../controllers/settings');
const checkAuth = require("../middleware/check-auth");

router.get("", checkAuth, SettingsController.getSettings);
router.post("", checkAuth, SettingsController.setSetting);

module.exports = router;
