import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  // Global dark mode status.
  private isDarkModeEnabled = new BehaviorSubject<boolean>(false);
  darkMode = this.isDarkModeEnabled.asObservable();
  backendUrl = environment.backend_url;

  constructor(
    private http: HttpClient,
    private authService: AuthService
  ) { }

  // todo shared state
  getSettings(): Observable<({ unit: number, darkMode: boolean, zenMode: boolean })> {
    if (this.authService.getIsAuth()) {
      return this.http.get<{ unit: number, darkMode: boolean, zenMode: boolean }>(this.backendUrl + '/settings');
    } else {
      // Default observable.
      return of({ unit: 0, darkMode: false, zenMode: false});
    }
  }

  // todo error handling
  setSetting(setting: string, value: any): void {
    this.http.post(this.backendUrl + '/settings', { setting, value }).subscribe(resp => {
    }, err => console.log('HTTP Error', err));
  }

  toggleDarkMode(value?: boolean): void {
    if (value !== undefined) {
      this.isDarkModeEnabled.next(value);
    } else {
      this.isDarkModeEnabled.next(!this.isDarkModeEnabled.value);
    }
  }

  getDarkMode(): boolean {
    return this.isDarkModeEnabled.value;
  }
}
