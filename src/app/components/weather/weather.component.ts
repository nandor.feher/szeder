import { AfterViewChecked, AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ChangeDetectorRef } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { DragScrollComponent } from 'ngx-drag-scroll';
import { Observable, Subscription } from 'rxjs';
import { filter, find, map, pluck, tap } from 'rxjs/operators';
import { AuthService } from 'src/app/auth/auth.service';
import { City } from 'src/app/models/city.model';
import { WeatherData } from 'src/app/models/weather-data.model';
import { CitiesService } from 'src/app/services/cities.service';
import { FavoritesService } from 'src/app/services/favorites.service';
import { SettingsService } from 'src/app/services/settings.service';
import { WeatherService } from 'src/app/services/weather.service';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit, AfterViewChecked, OnDestroy {

  // City to be viewed.
  inputCity: City;
  // Condition to show heart next to city name.
  isCityFavorite = false;
  // Default blue background color.
  backgroundColor = '100% 180.31% at 0% 100%, #8DB2CC 0%, #3681C8 48.96%, #0042A0 100%';
  // Stored weather data.
  weather: WeatherData;
  // Default settings.
  settingUnit = 0;
  userIsAuthenticated = false;
  settingDarkModeEnabled = false;
  settingZenModeEnabled = false;
  // Condition for slideshow indicator dots.
  scrollIndex = 0;

  // Drag element
  @ViewChild('scroll', {read: DragScrollComponent}) ds: DragScrollComponent;

  // Subscriptions
  authListenerSubscription: Subscription;
  currentCitySub: Subscription;
  weatherSub: Subscription;
  darkModeSub: Subscription;
  authSub: Subscription;

  // Observables
  unit$: Observable<number>;
  darkMode$: Observable<boolean>;
  zenMode$: Observable<boolean>;
  backgroundColor$: Observable<string>;

  constructor(
    private titleService: Title,
    public weatherService: WeatherService,
    private favoritesService: FavoritesService,
    private citiesService: CitiesService,
    public settingsService: SettingsService,
    private cdRef: ChangeDetectorRef,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    // Set title of page using the built in Title service.
    this.titleService.setTitle('WEATHER | SZEDER');

    // Ask the backend for current city (BehaviorSubject) changes.
    this.currentCitySub = this.citiesService.currentCity.subscribe((currentCity: City) => {
      this.inputCity = currentCity;
      // If logged in.
      if (this.authService.getIsAuth()) {
        // Get favorites of user.
        /*this.favoritesService.getFavorites().pipe(
          tap(console.log),
          filter(cities => cities.find(city => currentCity.name === city.name)
        ));*/

        this.favoritesService.getFavorites().subscribe(cities => {
          // If favorites contain current city, display heart.
          cities.forEach(item => {
            if (currentCity !== null && item.name === currentCity.name) {
              this.isCityFavorite = true;
            }
          });
        });
      }
    });

    // Request weather for input city.
    this.weatherSub = this.weatherService.getWeather(this.inputCity.lat, this.inputCity.lon).subscribe((weatherData: WeatherData) => {
      this.weather = weatherData;
      // Set weather background color.
      this.setBackground(weatherData.weather.condition);
      this.cdRef.markForCheck();
    });

    // Check if dark mode is enabled.
    this.settingDarkModeEnabled = this.settingsService.getDarkMode();

    // Check if user is authenticated.
    this.userIsAuthenticated = this.authService.getIsAuth();

    // If logged out, clear input city and dark mode.
    this.authSub = this.authListenerSubscription = this.authService.getAuthStatusListener().subscribe((isAuthenticated: boolean) => {
        this.userIsAuthenticated = isAuthenticated;
        this.clearInputCity();
        this.settingsService.toggleDarkMode(false);
    });

    this.unit$ = this.getSettings().pipe(
      pluck('unit')
    );

    this.darkMode$ = this.getSettings().pipe(
      pluck('darkMode')
    );

    this.zenMode$ = this.getSettings().pipe(
      pluck('zenMode')
    );

    this.backgroundColor$ = this.weatherService.currentBackground;

  }

  // Detect changes (used for *ngIf and [ngStyle] directives).
  ngAfterViewChecked(): void {
    this.cdRef.detectChanges();
  }

  // Helpers
  getSettings(): Observable<{
    unit: number;
    darkMode: boolean;
    zenMode: boolean;
  }> {
    return this.settingsService.getSettings();
  }

  // Get date in given timezone
  getDate(timezone: string): string {
    let tempValue: string;
    // Format time using timezone.
    const formattedDate = new Date().toLocaleString('en-US', { timeZone: timezone });
    const date = new Date(formattedDate);

    const dayOfWeek = date.getDay();
    // Get day n-th item from days array and add time.
    tempValue = isNaN(dayOfWeek) ? null :
    ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'][dayOfWeek];
    tempValue += ' ' + date.getHours() + ':' + (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
    return tempValue;
  }

  // Returns whether time is day or night in given timezone.
  isDay(timezone: string): boolean {
    const formattedDate = new Date().toLocaleString('en-US', { timeZone: timezone });
    const date = new Date(formattedDate);

    const hours = new Date(date).getHours();
    return hours > 6 && hours < 20;
  }

  getDayOfWeek(timestamp: number): string {
    const date = new Date();
    date.setTime(timestamp * 1000);
    const dayOfWeek = date.getDay();
    return isNaN(dayOfWeek) ? null :
    ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'][dayOfWeek];
  }

  convertTemperature(value: number, unit: number): string {
    if (unit) {
      // Fahrenheit
      return Math.round(((value - 273.15) * 1.8) + 32) + '°F';
    } else {
      // Celsius
      return Math.round(value - 273.15) + '°C';
    }
  }

  // Convert wind data to specific units.
  getWind(value: number): string {
    return this.settingUnit === 0 ? value + 'km/h' : Math.round(value * 0.621371 * 100) / 100 + 'mph';
  }

  // Convert visibility data to specific units.
  getVisibility(value: number): string {
    return this.settingUnit === 0 ? (value / 1000) + ' km' : Math.round(value * 0.000621371 * 100) / 100 + ' miles';
  }

  // Set global background color.
  setBackground(condition: string): void {
    this.weatherService.changeBackground(this.getBackgroundForCondition(condition, this.isDay(this.weather.weather.timezone)));
  }

  getBackgroundForCondition(condition: string, isDay: boolean): string {
    switch (condition) {
      case 'Clear':
        return isDay ?
        '100% 180.31% at 0% 100%, #485661 0%, #6B7C8C 48.96%, #B5C3D0 100%' :
        '100% 180.31% at 0% 100%, #353A8F 0%, #0A174A 48.96%, #040E31 100%';
      case 'Clouds':
      case 'Haze':
        return isDay ?
        '100% 180.31% at 0% 100%, #485661 0%, #6B7C8C 48.96%, #B5C3D0 100%' :
        '100% 180.31% at 0% 100%, #273540 0%, #354C62 48.96%, #3D6489 100%';
      case 'Rain':
      case 'Thunderstorm':
        return isDay ?
        '100% 180.31% at 0% 100%, #858E99 0%, #6370A0 48.96%, #35468B 100%' :
        '100% 180.31% at 0% 100%, #90A6BF 0%, #3C4B84 48.96%, #0D1A4D 100%';
      case 'Snow':
        return isDay ?
        '100% 180.31% at 0% 100%, #EBEDF0 0%, #C9D1EE 48.96%, #AEBFFF 100%' :
        '100% 180.31% at 0% 100%, #AEB6BF 0%, #757D9E 48.96%, #515D8F 100%';
      default:
        return '100% 180.31% at 0% 100%, #485661 0%, #6B7C8C 48.96%, #B5C3D0 100%';
    }
  }

  getIconForCondition(condition: string): string {
    switch (condition) {
      case 'Clear':
        return this.isDay(this.weather.weather.timezone) ? 'clear_d' : 'clear_n';
      case 'Clouds':
        return 'clouds';
      case 'Rain':
        return 'rain';
      case 'Thunderstorm':
        return 'thunderstorm';
      case 'Haze':
        return 'haze';
      case 'Snow':
        return 'snow';
      default:
        return 'clouds';
    }
  }

  getColorForCondition(condition: string): string {
    switch (condition) {
      case 'Clear':
        return '#FFE600';
      case 'Clouds':
      case 'Haze':
      case 'Rain':
        return '#9BB9CE';
      case 'Thunderstorm':
        return '#FFE600';
      case 'Snow':
        return '#FFFFFF';
      default:
        return '#9BB9CE';
    }
  }

  toggleFavorite(): void {
    if (this.isCityFavorite) {
      this.isCityFavorite = false;
      this.favoritesService.deleteFavorite(this.inputCity.name);
    } else {
      this.isCityFavorite = true;
      this.favoritesService.addFavorite(this.inputCity.name, this.inputCity.lat, this.inputCity.lon);
    }
  }

  clearInputCity(): void {
    this.citiesService.changeInputCity(null);
  }

  dragIndexChanged(event: any): void {
    this.scrollIndex = event;
    this.cdRef.markForCheck();
  }

  ngOnDestroy(): void {
    this.authListenerSubscription.unsubscribe();
    this.currentCitySub.unsubscribe();
    this.weatherSub.unsubscribe();
    this.authSub.unsubscribe();
  }
}
