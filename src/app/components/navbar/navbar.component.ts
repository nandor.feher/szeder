import { ChangeDetectionStrategy } from '@angular/core';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavbarComponent implements OnInit, OnDestroy  {

  // Auth status and subscription.
  userIsAuthenticated = false;
  private authListenerSubscription: Subscription;

  constructor(
    private authService: AuthService
    ) {}

  ngOnInit(): void {
    // Subscribe to the auth observable.
    this.userIsAuthenticated = this.authService.getIsAuth();
    this.authService.getAuthStatusListener().subscribe((isAuthenticated: boolean) => {
      // This is used to display either 'Sign in' or 'log in'.
        this.userIsAuthenticated = isAuthenticated;
    });
  }

  onLogout(): void {
    this.authService.logout();
  }

  ngOnDestroy(): void {
    this.authListenerSubscription.unsubscribe();
  }

}
