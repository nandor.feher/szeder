import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AuthService } from '../auth/auth.service';
import { City } from '../models/city.model';

@Injectable({
  providedIn: 'root'
})
export class FavoritesService {

  backendUrl = environment.backend_url;

  constructor(
    private http: HttpClient,
    private authService: AuthService
  ) { }

  getFavorites(): Observable<City[]> {
    if (this.authService.getIsAuth()) {
      return this.http.get<City[]>(this.backendUrl + '/favorites');
    } else {
      // Default observable.
      return of(null);
    }
  }

  isFavorite(cityName: string): Observable<{ check }> {
    if (this.authService.getIsAuth()) {
      return this.http.get<{ check }>(this.backendUrl + '/favorites?city=' + cityName);
    } else {
      return of({ check: false });
    }
  }

  addFavorite(name: string, latitude: number, longitude: number): void {
    this.http.post(this.backendUrl + '/favorites', { name, latitude, longitude }).subscribe(resp => {
      console.log(resp);
    });
  }

  deleteFavorite(name: string): void {
    this.http.delete(this.backendUrl + '/favorites/' + name).subscribe(resp => {
      console.log(resp);
    });
  }

  deleteFavorites(): void {
    this.http.delete(this.backendUrl + '/favorites').subscribe(resp => {
      console.log(resp);
    });
  }
}
