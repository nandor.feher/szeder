const jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {
  try {
    // Extract token from Bearer
    const token = req.headers.authorization.split(" ")[1];
    // JWT verification if token is valid.
    const decodedToken = jwt.verify(token, 'b96b901c731d3fc66adadbac51a97674');
    req.userData = { email: decodedToken.email, userId: decodedToken.userId };
    next();
  } catch (error) {
    res.status(401).json({ message: "You are not authenticated!" });
  }
};
