import { CanActivate, ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router) {}

  // This is a service with which we can protect the provided routes from being accessed when a certain condition is met.
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean | Observable<boolean> | Promise<boolean> {
    // Check if user is authenticated.
    const isAuthenticated = this.authService.getIsAuth();

    // If not authenticated, navigate to the sign in route.
    if (!isAuthenticated) {
      this.router.navigate(['/auth/signin']);
    }
    return isAuthenticated;
  }
}
