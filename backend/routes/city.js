const express = require("express");
const router = express.Router();
const CityController = require('../controllers/city');

router.get("", CityController.getCity);

module.exports = router;
