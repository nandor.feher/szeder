const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const User = require("../models/user");

exports.createUser = async (req, res, next) => {
  try {

    if (await User.exists({ email: req.body.email })) {
      // Email address is already registered.
      return res.sendStatus(409);
    }

    // Hash password
    const hash = await bcrypt.hash(req.body.password, 10);
    
    // New user using the hashed password and default settings.
    const user = new User({
      email: req.body.email,
      password: hash,
      settings: {
        unit: 0,
        darkMode: false,
        zenMode: false
      },
      favorites: []
    });
    
    // Save the user.
    const result = await user.save();

    return res.sendStatus(201);
  } catch (error) {
    res.sendStatus(500);
  }
}

exports.userLogin = async (req, res, next) => {
  try {
    let fetchedUser = await User.findOne({ email: req.body.email });
    
    if (!fetchedUser) {
      return res.sendStatus(404);
    }

    // Check if password matches the stores one.
    if(!await bcrypt.compare(req.body.password, fetchedUser.password)) {
      return res.sendStatus(401);
    };
    
    // Sign new JWT token and send it.
    const token = jwt.sign(
      { email: fetchedUser.email, userId: fetchedUser._id },
      "b96b901c731d3fc66adadbac51a97674",
      { expiresIn: "1h" }
    );
    return res.status(200).json({
      token: token,
      expiresIn: 3600,
      userId: fetchedUser._id
    });
  } catch (error) {
    return res.sendStatus(401);
  }
}