import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { City } from 'src/app/models/city.model';
import { CitiesService } from 'src/app/services/cities.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { WeatherService } from 'src/app/services/weather.service';
import { FavoritesService } from 'src/app/services/favorites.service';
import { AuthService } from 'src/app/auth/auth.service';
import { SettingsService } from 'src/app/services/settings.service';
import { Title } from '@angular/platform-browser';
import { fromEvent, Observable, Subject, Subscription } from 'rxjs';

import {
  debounceTime,
  pluck,
  distinctUntilChanged,
  switchMap,
  catchError, tap, map, filter
} from 'rxjs/operators';


@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InputComponent implements OnInit, OnDestroy {

  // Geolocation of the user.
  geolocationPosition: any;

  searchForm: FormGroup;
  autoCompleteText: string;

  // Time lock of the request so that it doesn't send too many requests in a short period of time.
  requestLocked = false;
  backgroundColor = '';
  favorites: City[] = [];
  settingDarkModeEnabled = false;

  // Subscriptions
  settingsSub: Subscription;
  favoritesSub: Subscription;
  authListenerSubscription: Subscription;
  @ViewChild('search', { static: true }) searchField: ElementRef;

  // Observables
  backgroundColor$: Observable<string>;

  constructor(
    private titleService: Title,
    private citiesService: CitiesService,
    private formBuilder: FormBuilder,
    public weatherService: WeatherService,
    private favoritesService: FavoritesService,
    private authService: AuthService,
    private settingsService: SettingsService,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit(): void {

    // Set title of page using the built in Title service.
    this.titleService.setTitle('WEATHER | SZEDER');

    // Build the search form.
    this.searchForm = this.formBuilder.group({
      search: ''
    });

    fromEvent(this.searchField.nativeElement, 'keyup').pipe(
      debounceTime(500),
      pluck('target', 'value'),
      distinctUntilChanged(),
      map(searchTerm => {
        if (searchTerm.toString().length === 0) {
          this.autoCompleteText = '';
          this.cdr.markForCheck();
          return null;
        } else {
          return searchTerm;
        }
      }),
      filter(searchTerm => searchTerm !== null),
      switchMap(searchTerm => this.citiesService.getCitiesBySubstring(searchTerm.toString())),
      ).subscribe((response: City) => {
        this.autoCompleteText = response.name;
        this.cdr.markForCheck();
      });

    // Get dark mode from user
    this.settingsSub = this.settingsService.getSettings().subscribe(response => {
      this.settingDarkModeEnabled = response.darkMode;
    });

    // If authenticated, get favorites from the user.
    if (this.authService.getIsAuth()) {
      this.favoritesSub = this.favoritesService.getFavorites().subscribe(cities => {
        this.favorites = cities;
        this.cdr.markForCheck();
      });
    }

    // When the user logs out, delete favorites and dark mode.
    this.authListenerSubscription = this.authService.getAuthStatusListener().subscribe((isAuthenticated: boolean) => {
      if (!isAuthenticated) {
        this.favorites = [];
        this.settingDarkModeEnabled = false;
      }
    });

    // Setting the observable
    this.backgroundColor$ = this.weatherService.currentBackground;
  }

  @HostListener('window:keydown', ['$event'])
  handleKeyDown(event: KeyboardEvent): void {
    if (event.key === 'Enter') {
      this.submitSearch();
    }
  }

  // Send search data to service.
  submitSearch(city?: City): void {
    // Only if city is in parameter.
    if (city !== undefined) {
      // Change input city to parameter city.
      this.citiesService.changeInputCity(city);
      return;
    }

    // If no parameter is given, submit the auto complete text.
    // tslint:disable-next-line: no-shadowed-variable
    this.citiesService.getCity(this.autoCompleteText).subscribe((city: City) => {
      // Change input city to received city.
      this.citiesService.changeInputCity(city);
    });
  }

  // Request location from user.
  requestLocation(): void {
    if (window.navigator && window.navigator.geolocation) {
      window.navigator.geolocation.getCurrentPosition(position => {
        // Send coordinates to backend and change the input city to the result city.
        this.geolocationPosition = position;
        this.citiesService.getCity(null, position.coords.latitude, position.coords.longitude).subscribe((city: City) => {
          this.citiesService.changeInputCity(city);
        });
      }, error => {
        switch (error.code) {
          case 1:
            console.log('Permission Denied');
            break;
          case 2:
            console.log('Position Unavailable');
            break;
          case 3:
            console.log('Timeout');
            break;
          }
        }
      );
    }
  }

  deleteFavorite(cityName: string, index: number): void {
    this.favorites.splice(index, 1);
    this.favoritesService.deleteFavorite(cityName);
  }

  ngOnDestroy(): void {
    this.settingsSub.unsubscribe();
    this.authListenerSubscription.unsubscribe();
  }

}
