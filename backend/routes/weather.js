const express = require("express");
const router = express.Router();
const WeatherController = require('../controllers/weather');
const checkAuth = require("../middleware/check-auth");

router.get("", WeatherController.getWeather);

module.exports = router;
