import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ModalService } from 'src/app/services/modal.service';
import { WeatherService } from 'src/app/services/weather.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SignInComponent implements OnInit, OnDestroy {

  // Background color subscription.
  backgroundSub: Subscription;
  backgroundColor = '';

  // Modal
  modalTitle: string;
  modalDescription: string;
  modalButtonLabel: string;

  constructor(
    public weatherService: WeatherService,
    public authService: AuthService,
    private titleService: Title,
    private router: Router,
    private modalService: ModalService,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    // Set title of page using the built in Title service.
    this.titleService.setTitle('LOG IN | SZEDER');

    // Subscribe to background color changes.
    this.backgroundSub = this.weatherService.currentBackground.subscribe(color => {
      this.backgroundColor = color;
    });
  }

  login(form: NgForm): void {
    if (form.invalid) {
      return;
    }
    this.authService.login(form.value.email, form.value.password).then(status => {
      if (status) {
        switch (status) {
          case 404:
            form.reset();
            this.openModal('modal-single-option', 'No account found.', 'Sorry, we couldn\'t find an account with this email address.', 'Okay');
            break;
          case 401:
            form.reset();
            this.openModal('modal-single-option', 'Incorrect password.', 'The password you provided was incorrect. Please try again.', 'Okay');
            break;
          default:
            break;
        }
        this.cdr.markForCheck();
      }
    });
  }

  onSignup(form: NgForm): void {
    if (form.invalid || form.value.password !== form.value.password2) {
      return;
    }
    this.authService.createUser(form.value.email, form.value.password).subscribe(status => {
      // this.isLoading = false;
      switch (status) {
        case 201:
          this.openModal('modal-single-option', 'Successful registration!', 'You have been successfully registered. Now you can log in to your account.', 'Okay');
          break;
        case 409:
          this.openModal('modal-single-option', 'User already exists.', 'We found an account linked with this email address. Please use a different one.', 'Okay');
          break;
        default:
          this.openModal('modal-single-option', 'Unexpected error occurred.', 'Something went wrong on our end. Please refresh the website and try again.', 'Okay');
          break;
      }
      this.cdr.markForCheck();
    });
  }

  openModal(id: string, title: string, description: string, buttonLabel: string): void {
    this.modalTitle = title;
    this.modalDescription = description;
    this.modalButtonLabel = buttonLabel;
    this.modalService.open(id);
  }

  closeModal(id: string): void {
    this.modalService.close(id);
  }

  // Unsubscribe from Observables.
  ngOnDestroy(): void {
    this.backgroundSub.unsubscribe();
  }
}
