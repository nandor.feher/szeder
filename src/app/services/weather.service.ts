import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { WeatherData } from '../models/weather-data.model';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  // BehaviorSubject, default background color.
  private backgroundSource = new BehaviorSubject('100% 180.31% at 0% 100%, #8DB2CC 0%, #3681C8 48.96%, #0042A0 100%');
  currentBackground = this.backgroundSource.asObservable();
  backendUrl = environment.backend_url;

  constructor(
    private http: HttpClient
  ) { }

  changeBackground(message: string): void {
    this.backgroundSource.next(message);
  }

  getWeather(lat: number, lon: number): Observable<WeatherData> {
    if (!(lat && lon)) {
      return;
    }
    return this.http.get<WeatherData>
    (this.backendUrl + '/weather?lat=' + lat + '&lon=' + lon);
  }

}
