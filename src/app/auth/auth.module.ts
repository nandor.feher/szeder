import { NgModule } from '@angular/core';
import { SignInComponent } from './sign-in/sign-in.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AuthRoutingModule } from './auth-routing.module';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { SharedModule } from '../shared/shared.module';

// This module is loaded lazily so that when a user opens the application,
// it doesn't load this particular part, in order to save resources.
@NgModule({
    declarations:
        [SignInComponent],
    imports: [
        CommonModule,
        FormsModule,
        AuthRoutingModule,
        AngularSvgIconModule.forRoot(),
        SharedModule
    ]
  })
  export class AuthModule {
  }
