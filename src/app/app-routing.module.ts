import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';
import { MainPageComponent } from './components/main-page/main-page.component';
import { SettingsComponent } from './components/settings/settings.component';
import { WeatherComponent } from './components/weather/weather.component';

const routes: Routes = [
  // Parent-child routes
  { path: '', component: MainPageComponent, children: [
    { path: 'weather', component: WeatherComponent }
  ]},
  // Protected with AuthGuard
  { path: 'settings', component: SettingsComponent, canActivate: [AuthGuard] },
  // Lazy loaded module
  { path: 'auth', loadChildren: () => import('src/app/auth/auth.module').then(m => m.AuthModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
